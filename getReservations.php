<?php
    require "php/dbConn.php";

    //only accessed via ajax call, get all reservations from a given date and section
    if(!empty($_POST["date"])) {
        $date = $_POST["date"];
        $section = $_POST["section"];

        $conn = new DBConn();
        $reservations = $conn->getReservations($date, $section);
        $conn->close();

        echo json_encode($reservations);
    }
?>