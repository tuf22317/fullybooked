<?php
    require "php/dbConn.php";
    require "php/formValidate.php";

    	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);

    session_start();
    //if already logged on, return to index
    if (isset($_SESSION["username"])) {
        header("Location: index.php");
    }

    //instantiate form values and errors
    $username = "";
    $userErrors = "";
    $passErrors = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = FormValidate::checkInput($_POST["username"]);
        $password = FormValidate::checkInput($_POST["password"]);

        $conn = new DBConn();
        $user = $conn->getUser($username, $password);
        $conn->close();
        if (count($user) == 0) {
            $userErrors = "That username does not exist.";
        } else {
            if (password_verify($password, $user[0]["pass"])) {
                $_SESSION["username"] = $user[0]["username"];
                $_SESSION["permission_level"] = $user[0]["permission_level"];
                header("Location: index.php");
            } else {
                $passErrors = "That password is incorrect.";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fully Booked - Login</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="formContainer">
            <form class="form" action="login.php" method="post">
                <a href="index.php">
                    <img id="logo" src="res/logoHead.png">
                </a>
                <div>
                    <span class="error"><?php echo $userErrors; ?></span>
                    <input type="text" name="username" placeholder="Username" value="<?php echo $username; ?>" required autofocues>
                </div>
                <div>
                    <span class="error"><?php echo $passErrors; ?></span>
                    <input type="password" name="password" placeholder="Password" required>
                </div>
                <div>
                    <button class="formButton" type="submit" name="submit">Login</button>
                </div>
            </form>
        </div>
    </body>
</html>