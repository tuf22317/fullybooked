<?php 
    //if logged in, destroy session, return to index either way
    session_start();
    if (isset($_SESSION["username"])) {
        session_destroy();
    }
    header("Location: index.php");
?>