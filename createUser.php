<?php
    require "php/formValidate.php";
    require "php/dbConn.php";

    session_start();
    //if not logged in, go to login page
    if (!isset($_SESSION["username"])) {
        header("Location: login.php");
    } else if ($_SESSION["permission_level"] < 3) {
        header("Location: index.php?permissions=1");
    }

    $username = "";
    $permission_level = "";

    $userErrors = "";
    $passErrors = "";
    $permErrors = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = FormValidate::checkInput($_POST["username"]);
        $password = FormValidate::checkInput($_POST["password"]);
        $permission_level = FormValidate::checkInput($_POST["permission_level"]);

        $conn = new DBConn();
        $userErrors = FormValidate::checkUser($username, $conn);
        $passErrors = FormValidate::checkPassword($password);
        $permErrors = FormValidate::checkPermission($permission_level);
        $conn->close();

        $errors = $userErrors . $passErrors . $permErrors;  
        if (strlen($errors) == 0) {
            $conn = new DBConn();
            $conn->createUser($username, $password, $permission_level);
            $conn->close();
            header("Location: index.php?post_create_user=1");
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fully Booked - Create User</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="formContainer">
            <form class="form" action="createUser.php" method="post">
                <a href="index.php">
                    <img id="logo" src="res/logoHead.png">
                </a>
                <div>
                    <span class="error"><?php echo $userErrors; ?></span>
                    <input type="text" name="username" placeholder="Username" value="<?php echo $username; ?>" maxlength="20" required autofocues>
                </div>
                <div>
                    <span class="error"><?php echo $passErrors; ?></span>
                    <input type="password" name="password" placeholder="Password" required>
                </div>
                <div>
                    <span class="error"><?php echo $permErrors; ?></span>
                    <select name="permission_level">
                        <option value="">Permission Level</option>
                        <option value="1" <?php if($permission_level == "1"){echo "selected";} ?>>User</option>
                        <option value="2" <?php if($permission_level == "2"){echo "selected";} ?>>Manager</option>
                        <option value="3" <?php if($permission_level == "3"){echo "selected";} ?>>Admin</option>
                    </select>
                </div>
                <div>
                    <button class="formButton" type="submit" name="submit">Create User</button>
                </div>
            </form>
        </div>
    </body>
</html>