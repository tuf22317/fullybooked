<?php
    
    class FormValidate {
        
        //Check all inputs first by removing slashes, white space, and html chars
        public static function checkInput($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        //Validate any first name inputs
        public static function checkFirstName($fname) {
            $errMsg = "";
            if (strlen($fname) == 0) {
                $errMsg .= "Please enter a first name.";
            }
            return $errMsg;
        }

        //Validate any last name inputs
        public static function checkLastName($lname) {
            $errMsg = "";
            if (strlen($lname) == 0) {
                $errMsg .= "Please enter a last name.";
            }
            return $errMsg;
        }

        //Validate email address, ensures valid format
        public static function checkEmail($email) {
            $errMsg = "";
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errMsg .= "Please enter a valid email address.";
            }
            return $errMsg;
        }

        //Validate phone, 10 digit number
        public static function checkPhone($phone) {
            $errMsg = "";
            $phone = preg_replace("/[^0-9]/", "", $phone); //remove all non-number characters
            if (strlen($phone) != 10) {
                $errMsg .= "Please enter a valid 10 digit phone number.";
            }
            return $errMsg;
        }

        //Validate date, ensures not in past
        public static function checkDate($date) {
            $errMsg = "";
            $date = date($date);
            $currentDate = date("Y-m-d");
            if ($date < $currentDate) {
                $errMsg .= "Reservations cannot be made for the past.";
            }
            return $errMsg;
        }

        //Validate start time 
        public static function checkStart($start) {
            $errMsg = "";
            if (strlen($start) == 0) {
                $errMsg .= "Please select a start time.";
            }
            return $errMsg;
        }

        //Validate end time, ensure not same or before start time
        public static function checkEnd($end, $start) {
            $errMsg = "";
            if (strlen($end) == 0) {
                $errMsg .= "Please select an end time.";
            } else if (!strlen($end) == 0 && $end == $start) {
                $errMsg .= "Start and end times cannot be the same.";
            } else if ($start > $end) {
                $errMsg .= "End time cannot come before start time.";
            }
            return $errMsg;
        }

        //Validate party size, ensure between 1 and 8
        public static function checkPartySize($partySize) {
            $errMsg = "";
            if (!is_numeric($partySize) || $partySize < 1 || $partySize > 8) {
                $errMsg .= "Please enter a number from 1 to 8.";
            }
        }

        //Validate username, ensure doesn't already exist and is proper length
        public static function checkUser($username, $conn) {
            $user = $conn->getUser($username);
            if (count($user) != 0) {
                return "That username already exists.";
            } else {
                if (strlen($username) < 5) {
                    return "Username must be at least 5 characters long.";
                } else if (strlen($username) > 20) {
                    return "Username cannot be longer than 20 characters.";
                }
            }
        }

        //Validate password, ensure proper length and character type requirements are met
        public static function checkPassword($password) {
            $errMsg = "";
            if(strlen($password) < 8){
                $errMsg .= "Password must be at least 8 charactes long.";
            }
            if(!preg_match("/[A-Z]/", $password)){   
                $errMsg .= " Password must contain at least one capital letter.";
            }
            if(!preg_match("/[a-z]/", $password)){  
                $errMsg .= " Password must contain at least one lower case letter.";
            }
            if(!preg_match("/[0-9]/", $password)){
                $errMsg .= " Password must contain at least one number.";
            }
            return $errMsg;
        }

        //Ensure permission level is selected
        public static function checkPermission($permission_level) {
            $errMsg = "";
            if (strlen($permission_level) == 0) {
                $errMsg .= "Please select a permission level.";
            }
            return $errMsg;
        }
    }
?>