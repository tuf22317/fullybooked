<?php 

    class DBConn {
        private $connection;

        //Create a connection to the database and assign to $connection
        public function __construct() {
            $config = parse_ini_file("../../private/config.ini"); //retrieve credentials from ini file
            $conn = sqlsrv_connect($config["servername"], ["Database"=>$config["dbname"], "UID"=>$config["username"], "PWD"=>$config["password"]]);
            if ($conn === false) {
                die("Unable to connect to the database.");
            } else {
                $this->connection = $conn;
            }
        }

        //Close $connection
        public function close() {
            sqlsrv_close($this->connection);
        }

        //Adds a customer where it is unknown if they already exist
        public function addCustomer($fname, $lname, $email, $phone) {
            $custID = $this->getCustID($fname, $lname, $phone);
            if ($custID) { //if customer already exists, return that customers id
                return $custID;
            } else { //otherwise create the customer
                $query = "INSERT INTO customers (fname, lname, email, phone)
                          VALUES (?, ?, ?, ?)";

                $params = array($fname, $lname, $email, $phone);
                $stmt = sqlsrv_prepare($this->connection, $query, $params);
                sqlsrv_execute($stmt);
                sqlsrv_free_stmt($stmt);

                return $this->getCustID($fname, $lname, $phone);
            }

        }

        //Look up a customer ID using their first name, last name, and phone number
        public function getCustID($fname, $lname, $phone) {
            $query = "SELECT cust_id FROM customers
                      WHERE fname = ? AND lname = ? AND phone = ?";

            $params = array($fname, $lname, $phone);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);

            if (count($results) == 0) {
                return false;
            } else {
                return $results[0]["cust_id"];
            }
        }

        //Look up the customer ID assigned to a given reservation
        public function getCustIdFromRes($resId) {
            $query = "SELECT cust_id FROM reservations
                      WHERE res_id = ?";

            $params = array($resId);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);

            return $results[0]["cust_id"];
        }

        //Update a customer
        public function updateCustomer($custId, $fname, $lname, $email, $phone) {
            $query = "UPDATE customers SET fname = ?, lname = ?, email = ?, phone = ?
                      WHERE cust_id = ?";

            $params = array($fname, $lname, $email, $phone, $custId);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);
            sqlsrv_free_stmt($stmt);
        }

        //Update a reservation
        public function updateReservation($resId, $date, $section, $table, $start, $end, $partySize, $notes) {
            if ($this->checkDoubleBookOnUpdate($date, $start, $end, $section, $table, $resId)) {
                return "There is already a different reservation booked for that table during those times.";
            } else {
                $query = "UPDATE reservations 
                          SET res_date = ?, section_number = ?, table_number = ?, start_time = ?, end_time = ?, party_size = ?, notes = ?
                          WHERE res_id = ?";
                
                $params = array($date, $section, $table, $start, $end, $partySize, $notes, $resId);
                $stmt = sqlsrv_prepare($this->connection, $query, $params);
                sqlsrv_execute($stmt);
                sqlsrv_free_stmt($stmt);
                return "Success";
            }
        }

        //Delete a reservation
        public function deleteReservation($resId) {
            $query = "DELETE FROM reservations WHERE res_id = ?";

            $params = array($resId);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);
            sqlsrv_free_stmt($stmt);
        }

        //Add a reservation
        public function addReservation($cust_id, $date, $section, $table, $start, $end, $partySize, $notes) {
            if ($this->checkDoubleBook($date, $start, $end, $section, $table)) {
                return "There is already a reservation booked for that table during those times.";
            } else {
                $query = "INSERT INTO reservations (cust_id, res_date, section_number, table_number, start_time, end_time, party_size, notes)
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                
                $params = array($cust_id, $date, $section, $table, $start, $end, $partySize, $notes);
                $stmt = sqlsrv_prepare($this->connection, $query, $params);
                sqlsrv_execute($stmt);
                sqlsrv_free_stmt($stmt);
                return "Success";
            }
        }

        //Check that a reservation is not attempting to overlap with another one
        public function checkDoubleBook($date, $start, $end, $section, $table) {
            $query = "SELECT res_id FROM reservations
                      WHERE res_date = ? AND section_number = ? AND table_number = ?
                      AND ((start_time <= ? AND end_time >= ?)
                      OR (end_time > ? AND end_time <= ?)
                      OR (start_time >= ? AND start_time < ?))";

            $params = array($date, $section, $table, $start, $end, $start, $end, $start, $end);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);

            if (count($results) == 0) {
                return false;
            } else {
                return true;
            }
        }

        //Check that a reservation is not attempting to update to overlap with another one (excluding itself)
        public function checkDoubleBookOnUpdate($date, $start, $end, $section, $table, $resId) {
            $query = "SELECT res_id FROM reservations
                      WHERE res_date = ? AND section_number = ? AND table_number = ? AND res_id != ?
                      AND ((start_time <= ? AND end_time >= ?)
                      OR (end_time > ? AND end_time <= ?)
                      OR (start_time >= ? AND start_time < ?))";

            $params = array($date, $section, $table, $resId, $start, $end, $start, $end, $start, $end);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);

            if (count($results) == 0) {
                return false;
            } else {
                return true;
            }
        }

        //Get all reservations in a given section on a given date
        public function getReservations($date, $section) {
            $query = "SELECT * FROM customers, reservations
                      WHERE customers.cust_id = reservations.cust_id
                      AND res_date = ? AND section_number = ?";

            $params = array($date, $section);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);
            return $results;
        }

        //Get a specific reservation by its ID
        public function getResById($resId) {
            $query = "SELECT * FROM customers, reservations
                      WHERE customers.cust_id = reservations.cust_id
                      AND res_id = ?";

            $params = array($resId);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);
            return $results;
        }

        //lookup a user by their username
        public function getUser($username) {
            $query = "SELECT * FROM users WHERE username = ?";

            $params = array($username);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);

            $results = [];
            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                $results[] = $row;
            }

            sqlsrv_free_stmt($stmt);
            return $results;
        }

        //create a new user
        public function createUser($username, $password, $permission_level) {
            $hashed_password = password_hash($password, PASSWORD_BCRYPT);
            
            $query = "INSERT INTO users (username, pass, permission_level)
                      VALUES (?, ?, ?)";

            $params = array($username, $hashed_password, $permission_level);
            $stmt = sqlsrv_prepare($this->connection, $query, $params);
            sqlsrv_execute($stmt);
        }
    }

?>