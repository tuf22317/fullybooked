const tablesPerSection = 5;
const openTime = 11;
const closeTime = 24;

//Generate the table for a given section
function generateTable(section) {
    let tableString = "<table id='section" + section + "'><tr><th></th>";
    for (let i = openTime; i <= closeTime - 1; i++) {
        if (i == openTime) {
            tableString += "<th>" + i + ":00am</th>";
        } else if (i > 12) {
            tableString += "<th>" + (i - 12) + ":00pm</th>";
        } else {
            tableString += "<th>" + i + ":00pm</th>";
        }    
    }
    tableString += generateTableRows();
    tableString += "</table>";
    return tableString;
}                    

//Generate the row for an individual table
function generateTableRows(table) {
    let rowString = "";
    for (let i = 1; i <= tablesPerSection; i++) {
        rowString += "<tr id='" + i + "'><td>Table " + i + "</td>";
        for (let i = 11; i <= 23; i++) {
            rowString += "<td id='" + i + "'></td>";
        }
        rowString += "</tr>";
    }
    return rowString;
}

//Pull reservations for given section and date and place in the table
function populateTable(section, date) {
    $.ajax({
        type: "POST",
        url: "getReservations.php",
        dataType: "json",
        data: {
            "date": date,
            "section": section
        },
        success: function(data) {
            data.forEach(reservation => {
                let table = reservation["table_number"];
                let start = reservation["start_time"];
                let end = reservation["end_time"];
                let fname = reservation["fname"];
                let lname = reservation["lname"];
                let email = reservation["email"];
                let phone = reservation["phone"];
                let partySize = reservation["party_size"];
                let notes = reservation["notes"];
                let date = reservation["res_date"]["date"].substring(0, 10);
                let id = reservation["res_id"];
                for (let i = start; i < end; i++) { //iterate over every time slot for the duration of the reservation
                    let cell = $("#" + table + " #" + i);
                    cell.addClass("resCell"); //add css to highlight cell
                    cell.click(function(){ //add onlick function to show reservation details
                        showDetails(fname, lname, email, phone, date, start, end, partySize, notes, id);
                    });
                    if (i == start) { //put firstname in first cell of reservation
                        cell.html(fname);
                        cell.css("text-align", "right");
                    }
                    if (i == start + 1) { //put lastname in second cell of reservation
                        cell.html(lname);
                        cell.css("padding-left", "0");
                        cell.css("text-align", "left");
                    }
                }
            });
        },
        error: function(thrownError) {
            console.log(thrownError);
        }
    });
}

//put reservation details in modal and display the modal
function showDetails(fname, lname, email, phone, date, start, end, partySize, notes, id) {
    let resDetails = "<div id='contactInfo'><h4>" + fname + " " + lname + "</h4>" +
                     email + "<br> " + phone + "<br></div>";
    resDetails += "<div id='dateInfo'><h4>" + date + "</h4>" + numToTime(start) + " - " + 
                  numToTime(end) + "</div>";
    resDetails += "<div id='partyInfo'><h4>Party Size: " + partySize + "</h4>" + notes; 
    $("#res-details").html(resDetails);
    $("#edit-button").click(function(){
        window.location.href = "editRes.php?id=" + id;
    });
    $("#delete-button").click(function(){
        window.location.href = "deleteRes.php?id=" + id;
    });
    $("#modal").show();
}

//convert integer to a time string
function numToTime(num) {
    let time = "";
    if (num > 12) {
        time += (num - 12) + ":00pm";
    } else {
        time += num + ":00am";
    }
    return time;
}
