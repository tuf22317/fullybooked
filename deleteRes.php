<?php
    require "php/dbConn.php";

    session_start();
    //if not logged in, go to login page
    if (!isset($_SESSION["username"])) {
        header("Location: login.php");
    } else if ($_SESSION["permission_level"] < 2) {
        header("Location: index.php?permissions=1");
    }

    //redirect from page unless brought here by modal button or form submit
    if (isset($_GET["id"])) {
        $conn = new DBConn();
        $resId = $_GET["id"];
        $reservation = $conn->getResById($resId);
        $conn->close();
        
        $fname = $reservation[0]["fname"];
        $lname = $reservation[0]["lname"];
        $email = $reservation[0]["email"];
        $phone = $reservation[0]["phone"];
        $date = $reservation[0]["res_date"]->format("Y-m-d");
        $section = $reservation[0]["section_number"];
        $table = $reservation[0]["table_number"];
        $start = $reservation[0]["start_time"];
        $end = $reservation[0]["end_time"];
        $partySize = $reservation[0]["party_size"];
        $notes = $reservation[0]["notes"];
    } else if ($_SERVER["REQUEST_METHOD"] == "POST") { 
        $conn = new DBConn();
        $conn->deleteReservation($_POST["resId"]);
        $conn->close();
        header("Location: index.php?post_delete=1");
    } else {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fully Booked - Delete Reservation</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="formContainer">    
            <form class="form" action="deleteRes.php" method="post">
                <a href="index.php">
                    <img id="logo" src="res/logoHead.png">
                </a>
                <input class="hidden-input" type="number" name="resId" value="<?php echo $resId; ?>">
                <div>
                    <h4>Are you sure you want to delete this reservation? This action cannot be undone.</h4>
                    <button class="formButton" type="submit" name="submit">Yes</button>
                </div>
            <form>
        </div>
    </body>
</html>