<?php
    session_start();
    //if not logged in, go to login page
    if (!isset($_SESSION["username"])) {
        header("Location: login.php");
    }

    //display different messages based on redirect
    $redirectText = "";
    if (isset($_GET["post_edit"])) {
        $redirectText = "Reservation successfully edited.";
    } else if (isset($_GET["post_insert"])) {
        $redirectText = "Reservation successfully added.";
    } else if (isset($_GET["post_delete"])) {
        $redirectText = "Reservation successfully deleted.";
    } else if (isset($_GET["post_create_user"])) {
        $redirectText = "User successfully created";
    } else if (isset($_GET["permissions"])) {
        $redirectText = "You do not have high enough permissions to view that page.";
    }

    //create today's date for default display
    $today = date("Y-m-d");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fully Booked - Home</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/script.js"></script>
        <script>
            $(document).ready(function(){
                $("#resTable").html(generateTable(1));
                populateTable(1, $("#date").val());

                $("#section").on("change", function(){
                    let section = $("#section").val();
                    let date = $("#date").val();
                    $("#resTable").html(generateTable(section));
                    populateTable(section, date);
                });

                $("#date").on("change", function(){
                    let section = $("#section").val();
                    let date = $("#date").val();
                    $("#resTable").html(generateTable(section));
                    populateTable(section, date);
                });

                $(window).click(function(e){
                    if (e.target.id == "modal") {
                        $("#modal").hide();
                    }
                });

                $("#close-button").click(function(){
                    $("#modal").hide();
                });
            });
        </script>
    </head>
    <body>
        <span id="logout">
            <a href="logout.php"><button type="button">Logout</button></a>
        </span>
        <?php if ($_SESSION["permission_level"] >= 3) { //If current user is an admin, give create user optioin
            echo "<span id='createUser'><a href='createUser.php'><button type='button'>Create User</button></a></span>";
        } ?>
        <div id="homeContainer">
            <span id="redirectText"><h4><?php echo $redirectText ?></h4></span>
            <div>
                <a href="index.php">
                    <img id="logo" src="res/logoHead.png">
                </a>    
                <br>
                <input id="date" type="date" name="date" value="<?php echo $today; ?>"><br><br>
                <select id="section">
                    <option value="1">Section 1</option>
                    <option value="2">Section 2</option>
                    <option value="3">Section 3</option>
                    <option value="4">Section 4</option>
                </select>
            </div>
            <div id="tableContainer">
                <div id="resTable"></div>
            </div>
            <button class="formButton" type="button" onclick="location.href='newRes.php';">New Reservation</button>
        </div>
        <div id="modal">
            <div id="modal-content">
                <div id="modal-options">
                    <span id="close-button">&times;</span>
                </div>
                <div id="res-details"></div>
                <div id="res-options">
                    <button id="edit-button" type="button">Edit</button>
                    <button id="delete-button" type="button">Delete</button>
                </div>
            </div>    
        </div>
    </body>
</html>