<?php
    require "php/formValidate.php";
    require "php/dbConn.php";

    session_start();
    //if not logged in, go to login page
    if (!isset($_SESSION["username"])) {
        header("Location: login.php");
    } else if ($_SESSION["permission_level"] < 2) {
        header("Location: index.php?permissions=1");
    }

    //redirect from page unless brought here by modal button or form submit
    if (isset($_GET["id"])) {
        $conn = new DBConn();
        $resId = $_GET["id"];
        $reservation = $conn->getResById($resId);
        $conn->close();
    
        //instantiate all form values
        $fname = $reservation[0]["fname"];
        $lname = $reservation[0]["lname"];
        $email = $reservation[0]["email"];
        $phone = $reservation[0]["phone"];
        $date = $reservation[0]["res_date"]->format("Y-m-d");
        $section = $reservation[0]["section_number"];
        $table = $reservation[0]["table_number"];
        $start = $reservation[0]["start_time"];
        $end = $reservation[0]["end_time"];
        $partySize = $reservation[0]["party_size"];
        $notes = $reservation[0]["notes"];

        //instnatiate all form errors
        $fnameErrors = "";
        $lnameErrors = "";
        $emailErrors = "";
        $phoneErrors = "";
        $dateErrors = "";
        $startErrors = "";
        $endErrors = "";
        $partySizeErrors = "";
        $notesErrors = "";

        //instantiate general errors
        $sqlErrors = "";
    } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //trim, strip slashes, and htmlspecialchar all inputs
        $fname = FormValidate::checkInput($_POST["fname"]);
        $lname = FormValidate::checkInput($_POST["lname"]);
        $email = FormValidate::checkInput($_POST["email"]);
        $phone = FormValidate::checkInput($_POST["phone"]);
        $date = FormValidate::checkInput($_POST["date"]);
        $section = FormValidate::checkInput($_POST["section"]);
        $table = FormValidate::checkInput($_POST["table"]);
        $start = FormValidate::checkInput($_POST["start"]);
        $end = FormValidate::checkInput($_POST["end"]);
        $partySize = FormValidate::checkInput($_POST["partySize"]);
        $notes = FormValidate::checkInput($_POST["notes"]);

        $resId = $_POST["resId"];

        //check custom validations for each input
        $fnameErrors = FormValidate::checkFirstName($fname);
        $lnameErrors = FormValidate::checkLastName($lname);
        $emailErrors = FormValidate::checkEmail($email);
        $phoneErrors = FormValidate::checkPhone($phone);
        $dateErrors = FormValidate::checkDate($date);
        $startErrors = FormValidate::checkStart($start);
        $endErrors = FormValidate::checkEnd($end, $start);
        $partySizeErrors = FormValidate::checkPartySize($partySize);
        $sqlErrors = "";

        $errors = $fnameErrors . $lnameErrors . $emailErrors . $phoneErrors . $dateErrors . $startErrors . $endErrors . $partySizeErrors;
        //if no errors, proceed to database functions
        if (strlen($errors) == 0) {
            $conn = new DBConn();
            $custId = $conn->getCustIdFromRes($resId);
            $conn->updateCustomer($custId, $fname, $lname, $email, $phone);
            $sqlErrors = $conn->updateReservation($resId, $date, $section, $table, $start, $end, $partySize, $notes);
            $conn->close();
            if ($sqlErrors == "Success") {
                header("Location: index.php?post_edit=1");
            }
        }
    } else {
        header("Location: index.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fully Booked - Edit Reservation</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="formContainer">
            <form class="form" action="editRes.php" method="post">
                <a href="index.php">
                    <img id="logo" src="res/logoHead.png">
                </a>
                <h4>Please edit the desired fields.</h4>
                <span class="error"><?php echo $sqlErrors; ?></span>
                <div>
                    <span class="error"><?php echo $fnameErrors; ?></span>
                    <input placeholder="First name" type="text" name="fname" maxlength="50" value="<?php echo $fname; ?>" required autofocus>
                </div>
                <div>
                    <span class="error"><?php echo $lnameErrors; ?></span>
                    <input placeholder="Last name" type="text" name="lname" maxlength="50" value="<?php echo $lname; ?>" required>
                </div>    
                <div>
                    <span class="error"><?php echo $emailErrors; ?></span>
                    <input placeholder="Email address" type="email" name="email" maxlength="50" value="<?php echo $email; ?>">
                </div> 
                <div>
                    <span class="error"><?php echo $phoneErrors; ?></span>
                    <input placeholder="Phone number" type="tel" name="phone" maxlength="10" value="<?php echo $phone; ?>" required>
                </div> 
                <div>
                    <span class="error"><?php echo $dateErrors; ?></span>
                    <input placeholder="Date" type="date" name="date" maxlength="50" value="<?php echo $date; ?>" required>
                </div> 
                <div>
                    <select name="section">
                        <option value="1" <?php if($section == "1"){echo "selected";} ?>>Section 1</option>
                        <option value="2" <?php if($section == "2"){echo "selected";} ?>>Section 2</option>
                        <option value="3" <?php if($section == "3"){echo "selected";} ?>>Section 3</option>
                        <option value="4" <?php if($section == "4"){echo "selected";} ?>>Section 4</option>
                    </select>
                </div> 
                <div>
                    <select name="table">
                        <option value="1" <?php if($table == "1"){echo "selected";} ?>>Table 1</option>
                        <option value="2" <?php if($table == "2"){echo "selected";} ?>>Table 2</option>
                        <option value="3" <?php if($table == "3"){echo "selected";} ?>>Table 3</option>
                        <option value="4" <?php if($table == "4"){echo "selected";} ?>>Table 4</option>
                        <option value="5" <?php if($table == "5"){echo "selected";} ?>>Table 5</option>
                    </select>
                </div> 
                <div>
                    <span class="error"><?php echo $startErrors; ?></span>
                    <select name="start">
                        <option value="">Start time</option>
                        <option value="11" <?php if($start == "11"){echo "selected";} ?>>11:00AM</option>
                        <option value="12" <?php if($start == "12"){echo "selected";} ?>>12:00PM</option>
                        <option value="13" <?php if($start == "13"){echo "selected";} ?>>1:00PM</option>
                        <option value="14" <?php if($start == "14"){echo "selected";} ?>>2:00PM</option>
                        <option value="15" <?php if($start == "15"){echo "selected";} ?>>3:00PM</option>
                        <option value="16" <?php if($start == "16"){echo "selected";} ?>>4:00PM</option>
                        <option value="17" <?php if($start == "17"){echo "selected";} ?>>5:00PM</option>
                        <option value="18" <?php if($start == "18"){echo "selected";} ?>>6:00PM</option>
                        <option value="19" <?php if($start == "19"){echo "selected";} ?>>7:00PM</option>
                        <option value="20" <?php if($start == "20"){echo "selected";} ?>>8:00PM</option>
                        <option value="21" <?php if($start == "21"){echo "selected";} ?>>9:00PM</option>
                        <option value="22" <?php if($start == "22"){echo "selected";} ?>>10:00PM</option>
                        <option value="23" <?php if($start == "23"){echo "selected";} ?>>11:00PM</option>
                    </select>
                </div>
                <div>
                    <span class="error"><?php echo $endErrors; ?></span>
                    <select name="end">
                        <option value="">End time</option>
                        <option value="12" <?php if($end == "12"){echo "selected";} ?>>12:00PM</option>
                        <option value="13" <?php if($end == "13"){echo "selected";} ?>>1:00PM</option>
                        <option value="14" <?php if($end == "14"){echo "selected";} ?>>2:00PM</option>
                        <option value="15" <?php if($end == "15"){echo "selected";} ?>>3:00PM</option>
                        <option value="16" <?php if($end == "16"){echo "selected";} ?>>4:00PM</option>
                        <option value="17" <?php if($end == "17"){echo "selected";} ?>>5:00PM</option>
                        <option value="18" <?php if($end == "18"){echo "selected";} ?>>6:00PM</option>
                        <option value="19" <?php if($end == "19"){echo "selected";} ?>>7:00PM</option>
                        <option value="20" <?php if($end == "20"){echo "selected";} ?>>8:00PM</option>
                        <option value="21" <?php if($end == "21"){echo "selected";} ?>>9:00PM</option>
                        <option value="22" <?php if($end == "22"){echo "selected";} ?>>10:00PM</option>
                        <option value="23" <?php if($end == "23"){echo "selected";} ?>>11:00PM</option>
                        <option value="24" <?php if($end == "24"){echo "selected";} ?>>12:00AM</option>
                    </select>
                </div>
                <div>
                    <span class="error"><?php echo $partySizeErrors; ?></span>
                    <input placeholder="Party size" type="number" name="partySize" min="1" max="8" value="<?php echo $partySize; ?>" required>
                </div>
                <div>
                    <textarea name="notes" maxelength="250"><?php echo $notes; ?></textarea>
                </div>
                <input class="hidden-input" type="number" name="resId" value="<?php echo $resId; ?>">
                <div>
                    <button class="formButton" type="submit" name="submit">Edit Reservation</button>
                </div>
            </form>
        </div>
    </body>
</html>